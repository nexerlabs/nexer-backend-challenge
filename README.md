# Desafio Técnico Python #

### Introdução ###

O objetivo do desafio é esclarecer o nivel de compreensão de HTTP, REST, microserviços, integração entre sistemas, alguns dos conceitos que empregamos rotineiramente na Nexer. A ideia é criar uma API REST utilizando algum framework que se tenha conforto (preferencialmente, Django Rest Framework), e que se faça deploy deste microserviço em alguma VPS. Nossa sugestão pessoal é utilizar um plano gratuito do Heroku, pois usamos Heroku internamente.

A API desenvolvida será um sistema hipotético que faz a consulta de todos os carros que foram alugados em determinado dia, de uma locadora de veículos hipotetica. A API que você desenvolverá irá buscar os dados na API que esta locadora disponibilizou, armazenar em um banco de dados, e retornar para o usuário.

Quando algum usuário solicitar à sua API quais carros foram alugados em determinado dia, ela precisa usar os dados armazenados localmente caso eles existam e se foram armazenados em menos de 2 minutos, caso contrário deve-se buscar na API da locadora e armazenar novamente.

O recurso que a locadora dispõe segue o seguinte padrão. Apenas o método GET está disponivel, para consulta: https://demo8306178.mockable.io/hipotetica/car-renting/%Y-%m-%d/

(como esta é uma URL de Mock, apenas as seguintes datas estão disponiveis: 01/05/2017, 02/05/2017, 03/05/2017)

Para a API que será desenvolvida, os recursos REST são de sua livre escolha. Nós precisamos de um método GET que irá fazer a consulta na locadora, passando a data como parâmetro, e um método DELETE que irá apagar neste banco de dados interno para a data enviada como parametro.

Nosso maior interesse é entender como você organiza as peças desta lógica.



### Você deve Entregar ###

* Testes unitários
* Documentação
* Código limpo, legível, manutenível


### Processo de Submissão ###


Enviar um Pull Request para este repositório com a soluçao. Criar um fork deste repositório, desenvolva e envie um Pull Request